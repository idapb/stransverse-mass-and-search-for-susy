\documentclass[11pt]{article}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{a4wide}
%\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage[dvips]{epsfig}
%\usepackage[T1]{fontenc}
\usepackage{cite} % [2,3,4] --> [2--4]
%\usepackage{shadow}
%\usepackage[hidelinks]{hyperref}
\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

%\usepackage{slashed}
%\usepackage[norsk]{babel}
\usepackage[utf8]{inputenc}
\usepackage[nottoc]{tocbibind}

%\lstset{language=c++}
%\lstset{alsolanguage=[90]Fortran}
%\lstset{basicstyle=\small}
%\lstset{backgroundcolor=\color{white}}
%\lstset{frame=single}
%\lstset{stringstyle=\ttfamily}
%\lstset{keywordstyle=\color{red}\bfseries}
%\lstset{commentstyle=\itshape\color{blue}}
%\lstset{showspaces=false}
%\lstset{showstringspaces=false}
%\lstset{showtabs=false}
%\lstset{breaklines}
\begin{document}


%\title{FYS - 4560 Final Project}
\title{FYS-4560 Final Project: \\Stransverse mass and search for SUSY}
\author{Ida Pauline Buran}
\maketitle

\begin{abstract}
This project considers the use of the kinematic variable, $M_{T2}$, for supersymmetric processes with R-parity conservation at hadron colliders. The features of supersymmetry which are relevant for this project, including some motivation, is presented in the introduction. After a definition of $M_{T2}$, two examples of its use are presented. The first example considers extracting masses of pair produced particles, where each particle decays to one directly observable particle and one particle which escapes detection. The other example considers searching for supersymmetry using $M_{T2}$ as a cut variable which could discriminate against the majority of Standard Model background. Both examples are reasonably model independent and simulations suggest that the variable could be useful both for measuring masses and searching for new physics at hadron colliders.
\end{abstract}

%\tableofcontents

\newpage
\section*{Introduction}
The Standard Model (SM) is the most successful theory of particle physics to date. It classifies all the known subatomic particles and explains how they interact through the electromagnetic, weak and strong nuclear forces. The SM has succesfully explained almost all experimental results and precisely predicted a wide variety of phenomena. It is, however, an incomplete theory. There are several fundamental physical phenomena the SM can not explain. For example it does not include gravity, and is incompatible with Einstein's theory of general relativity which is the most succesful theory of gravity to date. 
Another problem in the SM is that the square of the Higgs mass gets some very large quantum corrections that would be expected to make the mass of the Higgs huge, unless the bare mass of the Higgs is incredibly fine tuned in a way that cancels these quantum corrections. This is known as the hierarchy problem \cite{susy}. %According to the SM, matter and antimatter should have been created in almost equal amounts, but the universe consists mostly of matter. 
The SM only explains 5\% of the energy in the universe, the part that is visible matter. 
According to cosmological observation, dark matter and dark energy make up about 95\% of the total energy content. 
\\\\
These are only a few examples of some of the deficiencies of the Standard Model.
Several theories describing new physics beyond the SM have been created in attempt to explain these deficiencies, one of the most popular being supersymmetry (SUSY), which will be described briefly in the next section. A major motivation for SUSY is that it resolves the hierarchy problem if the new particles in this theory are close to the TeV energy scale. Then the quantum corrections from the SUSY particles will cancel the ones from SM particles that contribute to the Higgs mass, making the light Higgs boson that we observe possible \cite{susy}. 
SUSY will also make a Grand Unified Theory (GUT) possible as it allows for the exact unification of the weak, strong and electromagnetic interactions at very high energies, as in the early universe \cite{Dine:1996ui}. 
In many SUSY models the lightest SUSY particle is stable, electrically neutral and interacts weakly with SM particles. This makes it a good candidate for dark matter \cite{Dine:1996ui}. 
When SUSY is imposed as a local symmetry, Einstein's theory of general relativity is included automatically, and the result is called a theory of supergravity \cite{Nastase:2011aa}.
SUSY is also a necessary feature for superstring theory, the most popular candidate for a theory of everything \cite{Dine:1996ui}.
\\\\
One of the primary goals at the Large Hadron Collider (LHC) at CERN is to search for new physics beyond the SM. If SUSY exists close to the TeV scale, it should be possible to detect traces of SUSY particles at the LHC. Nothing has been discovered yet which puts a significant constraint on the most popular SUSY models. A large part of the parameter space of SUSY has mostly been ruled out. However, the total parameter space is very diverse and can not be ruled out definitively by the LHC. If SUSY still exists close to the TeV scale, it might be necessary to look for more clever search strategies in order to make a discovery.
%If SUSY exists, it could be that either the SUSY-particles are too heavy to be detected, in which case the theory does not solve the deficiencies of the SM, or we need to look for new search strategies.
\\\\
In this project we will look at the kinematic variable stransverse mass, $M_{T2}$, and how it can be used as a search variable in the hunt for SUSY. This variable is well suited for SUSY processes in the most popular models because of the way the SUSY particles are produced, how they decay and because of their heavy masses.
% as it describes pair-produced particles which each decay to one visible and one invisible system. SUSY particles are pair-produced and decay to the lightest SUSY particle (LSP), which will be invisible to the detector.
Several ways of using $M_{T2}$ as a search variable has already been proposed in the literature and we will try to reproduce some of the results of one of these studies.  

\section*{Supersymmetry}
The theory in this section is taken from ref. \cite{Dine:1996ui}, \cite{susy} and \cite{msugra}.

Supersymmetry is an extension of the Standard Model which introduces a symmetry between fermions and bosons. Bosons are particles with integer-valued spin and fermions are particels with half-integer spin. According to SUSY, all SM particles have superpartners whose spin differ by one half from the ordinary particle. This means that bosons have fermionic superpartners and fermions have bosonic superpartners. These superpartners are called sparticles. 
 
If the theory was an unbroken supersymmetry, then each pair of superpartners would have equal mass and quantum numbers, except for the spin. However, since no sparticles have been observed yet, it must mean that the theory is a spontaneously broken symmetry and that sparticles are much heavier than their SM partners. This makes them harder to produce at hadron colliders and therefore harder to discover.
\\\\
No SM particles can be superpartners of each other. This means that the elementary particle content must be at least doubled in order to incorporate supersymmetry into the Standard Model. The Minimal Supersymmetric Standard Model (MSSM) is the simplest supersymmetric model consistent with the SM. It has the minimal particle content necessary to form superpartners for all SM particles. 

The particle content of MSSM is as follows:
The superpartners of fermions are called squarks and sleptons, or sfermions together, and have spin-0. The ``s'' at the beginning stands for ``scalar''. All squarks and sleptons have the same name as their superpartner only with an ``s'' in front, and they have the same symbol but with a tilde. So for example the electron superpartner is a selectron and is written $\tilde{e}$. Fermionic superpartners of bosons in the SM have ``ino'' at the end of the name. For example spin-1/2 superpartners of the gauge bosons are gauginos. There are five Higgs bosons in the MSSM with higgsino superpartners. The higgsinos and the electroweak gauginos mix and form four neutral mass eigenstates called neutralinos, $\tilde{\chi}_i^0$ for i = 1,2,3,4, and two charged mass eigenstates called charginos, $\tilde{\chi}_i^{\pm}$ for i = 1,2. Hence the particles with tilde do not exist in the non-supersymmetric Standard Model.
\\\\
The lagrangian of the superpotential contains terms which violate lepton or baryon numbers. This allows for the proton decay process $p \rightarrow e^+\pi^0$ with the lifetime of the proton predicted to be very short. However, the experimental limit on the proton partial lifetime in this mode is $\tau_p > 1.6 \times 10^{33}$ years. Clearly this is inconsistent unless the coupling constants are incredibly small. In order to solve this a discrete symmetry called R-parity is introduced. R-parity is defined as:
\[
P_R = (-1)^{3(B-L)+2s},
\]
where $B$ is baryon number, $L$ is lepton number and $s$ is spin. 

This means that all SM particles have even parity and SUSY particles have odd parity. When R-parity is imposed, the baryon and lepton violating terms in the lagrangian are forbidden. Hence the proton can not decay in R-parity conserving SUSY-models.

One immediate consequence of R-parity conservation is that the lightest supersymmetric particle (LSP) is stable, since its decay would violate R-parity. There are no odd parity particles for it to decay to. This makes it a very good candidate for cold dark matter. For the LSP to be a dark matter candidate it should be electrically neutral and non-strongly interacting, otherwise it should have been discovered already. In many supersymmetric models the LSP is the lightest neutralino, $\tilde{\chi}_1^0$. 

Another consequence of R-parity is that SUSY particles always have to be produced in pairs and they always cascade decay down to the LSP, which escapes detection. This is why the typical signature of SUSY in collider experiments is missing energy from the two LSP's at the end of the decay chain.
\\\\
There is a very large number of free parameters in the MSSM, 150 without R-parity conservation and 105 after R-parity conservation is imposed. It is therefore necessary to impose some boundary conditions that constrain these parameters in order to make any phenomenological predictions. One common choice is the model called minimal supergravity (mSUGRA), which combines the principles of supersymmetry and general relativity. It assumes that supersymmetry is a local symmetry and that the breaking of the symmetry is mediated by gravity. mSUGRA requires only 4 input parameters and a sign, all set at the GUT scale. The phenomenology is then evolved down to lower energies. The parameters are: 
\begin{equation*}
\begin{split}
m_0:\:& \text{the common mass of scalar particles}\\
m_{1/2}:\:& \text{the common mass of gauginos}\\
A_0:\:& \text{the trilinear Higgs-fermion-fermion coupling}\\
\tan\beta:&\: \text{the ratio of Higgs vacuum expectation values}\\
\text{sign}\:\mu:&\: \text{the sign of the higgsino mass parameter}
\end{split}
\end{equation*}
mSUGRA is one of the most studied SUSY models because of its predictive power requiring only these five parameters to determine the phenomenology.


\section*{Definition of $M_T2$}

From the energy-momentum relation 
\[
m^2 = E^2 - |\mathbf{p}|^2,
\]
where $m$ is mass, $E$ is energy and $\mathbf{p}$ is momentum, we can define the invariant mass of a two-particle decay as:
\begin{align*}
m_0^2 &= (E_1 + E_2)^2 - |\mathbf{p}_1 + \mathbf{p}_2|^2\\
&= m_1^2 + m_2^2 + 2(E_1E_2 - \mathbf{p}_1\cdot\mathbf{p}_2),
\end{align*}
where 1 and 2 refers to the two daughter particles.

The invariant mass of the daughter particles equals the rest mass of the mother particle. If all the daughter particles in a collider experiment are measured by the detector then we can use the invariant mass to directly estimate the mass of the mother particle. This is how the mass of the Z boson was determined from its decay into lepton pairs by the UA1 \cite{UA1:1983a,UA1:1983b} and UA2\cite{UA2:1983a,UA2:1983b} collaborations at CERN. 

However, if one of the particles escapes the detector we cannot estimate the mass directly. For example the W boson can decay into a charged lepton and a neutrino which is invisible to the detector. The transverse momentum of the invisible particle can be found from conservation of momentum in the plane perpendicular to the beam. It is assumed to be equal to the missing transverse momentum, which is defined to be the negative sum of the transverse momentum of all the observed particles. 
In order to measure the mass of the W boson UA1\cite{UA1:1983a} introduced the transverse mass variable:
\begin{equation*}
m_T^2 = m_1^2 + m_2^2 + 2(E_{T,1}{E}_{T,2} - \mathbf{p}_{T,1}\cdot\mathbf{p}_{T,2}),
\end{equation*}
where $E_T$ and $\mathbf{p}_T$ are energy and momentum in the transverse plane.

This variable has the property that
\[
m_T^2 \leq m_0^2
\]
From this we get a lower limit on the mass of the mother particle. For a large number of events we will get a distribution where the endpoint approaches the true value of the mass.
\\\\

\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth]{figur1.jpg}
  \caption{A hadronic collision leads to the pair production of two particles which each decay to one observed particle, with momenta $p_1$ and $p_2$ respectively, and one particle which escapes the detector leading to missing transverse momentum, $p_T^{miss}$.\label{fig:1}}
\end{figure}

The stransverse mass variable, $M_{T2}$, is an extension of transverse mass. It was introduced \cite{Lester:1999tx} to measure the masses of pair produced particles at hadron colliders, where both decay to one observed and one unobserved particle. The general process is shown in figure \ref{fig:1}. We assume for simplicity that the pair produced particles have equal mass. In R-parity conserving SUSY processes, sparticles are pair produced and cascade decay to the LSP, $\tilde{\chi}$, which is invisible to the detector. For this type of process we can define two transverse masses, one for each branch of the decay:
\begin{equation}
(m_T^{(i)})^2 = (m^{vis(i)})^2 + m_{\tilde{\chi}}^2 + 2(E_T^{vis(i)}{E}_T^{\tilde{\chi}(i)} - \mathbf{p}_T^{vis(i)}\cdot\mathbf{p}_T^{\tilde{\chi}(i)}),
\label{eq:2mt}
\end{equation}
where i = 1,2
\\\\
For the true values of the LSP mass and momenta, $m_{\tilde{\chi}}$ and $\mathbf{p}_{\tilde{\chi}}$ respectively, each of these transverse masses give a lower limit for the mass of the pair produced mother particles. We can therefore choose the largest one as a lower estimate:
\begin{equation}
m_0 \geq \text{max}(m_T^1,m_T^2),
\label{eq:inequality}
\end{equation}
where $m_0$ is the mass of the mother particle. \\\\
\\\\
As only the total missing transverse momentum is measured, the transverse momentum of each LSP is unkown. To make sure that the estimate does not exceed the mass of the mother particle, a minimization is performed over all possible momenta fulfilling the total missing transverse momentum constraint, $\mathbf{p}_T^{\tilde{\chi}(1)}+\mathbf{p}_T^{\tilde{\chi}(2)} = \mathbf{p}_T^{miss}$. This leads to the definition of stransverse mass:
\[
M_{T2}(m_{\tilde{\chi}}) = \underset{\mathbf{p}_T^{\tilde{\chi}(1)}+\mathbf{p}_T^{\tilde{\chi}(2)} = \mathbf{p}_T^{miss}}{\text{min}}\left[\text{max}(m_T^1,m_T^2)\right],
\]
which for SUSY processes is a function of $m_{\tilde{\chi}}$. For the true value of $m_{\tilde{\chi}}$ we get a distribution which has an endpoint at the mass of the mother particle. For processes where the pair produced particles have different mass, $M_{T2}$ gives a lower estimate for the heaviest mother particle.
\\\\
Calculating $M_{T2}$ analytically is non trivial but it has been done \cite{Cho:2007dh} for the case where initial state radiation (ISR) can be neglected. However, there are several programs made for computing $M_{T2}$ numerically which takes complications such as ISR into account.


\section*{Measuring masses with $M_{T2}$}
To demonstrate the behaviour of $m_{T2}$, we simulate proton-proton collisions at the LHC and calculate the $M_{T2}$ distribution for the process: $$pp \rightarrow X + \tilde{l}_R^+\tilde{l}_R^- \rightarrow X + l^+l^-\tilde{\chi}_1^0\tilde{\chi}_1^0$$ in the mSUGRA model at the parameter point: 
$$\tan\beta = 2.2,\: m_{1/2} = 300\: \text{GeV},\: m_0 = 100 \:\text{GeV},\: A_0 = 300\: \text{GeV},\: \mu > 0.$$ 
This parameter point is chosen because it allows the production of sleptons at the centre of mass energy of 7 TeV, and hence we expect a reasonable amount of signal events for the process considered when we perform simulations. The lightest neutralino, $\tilde{\chi}_1^0$, is the LSP for this model which escapes the detector.
\\\\
The SUSY particle spectrum has been generated using SPheno-3.3.4 \cite{Porod:2003um}. The only values from the spectrum which are needed for calculating $M_{T2}$ are the masses of the sleptons in the process, and the mass of the LSP, which are: $$m_{\tilde{l}_R} = 154.9 \:\text{GeV}\: \text{and}\: m_{\tilde{\chi}_1^0} = 115.7\:\text{GeV}.$$
1997 signal events have been generated using the Monte Carlo event generator PYTHIA 8.2 \cite{Sjostrand:2014zea} at the center of mass energy of 7 TeV. The $M_{T2}$ variable has been calculated for each process using the $M_{T2}$ calculator in ref \cite{Lester:2014yga}, with the correct value of $m_{\tilde{\chi}_1^0}$ as input to the invisible particle mass in the calculation. The result can be seen in figure \ref{fig:mt2}. 
\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{mt2.pdf}
  \caption{The stransverse mass distribution for the process $pp \rightarrow X + \tilde{l}_R^+\tilde{l}_R^- \rightarrow X + l^+l^-\tilde{\chi}_1^0\tilde{\chi}_1^0$. The events are generated with $m_{\tilde{l}_R} = 154.9$ GeV and $m_{\tilde{\chi}_1^0} = 115.7$ GeV. The correct value for $m_{\tilde{\chi}_1^0}$ has been used in the calculation. \label{fig:mt2}}
\end{figure}
\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{mass.pdf}
  \caption{Estimates for the mass of $m_{\tilde{l}_R}$ based on the maximum value of $M_{T2}$ where different values of $m_{\tilde{\chi}_1^0}$ has been used as input in the calculation.
The events are generated with $m_{\tilde{l}_R} = 154.9$ GeV and $m_{\tilde{\chi}_1^0} = 115.7$ GeV.\label{fig:mass}}
\end{figure}
%The $M{T2}$ distribution has also been calculated for a number of different values of $m_{\tilde{\chi}_1^0}$, and the maximum value in each case has been plotted as a function of $m_{\tilde{\chi}_1^0}$. This is shown in figure \ref{fig:mass}.
\\\\\\\\
From the $M_{T2}$ distribution in figure \ref{fig:mt2}, we see that the maximum value corresponds perfectly to the slepton mass. The distribution falls off gradually toward this value as expected. This edge is not as easy to measure as a vertical drop, but is potentially detectable experimentally. We also see that we get a lower bound which corresponds to the mass of the lightest neutralino. This can not be used to measure the value of $m_{\tilde{\chi}_1^0}$ however, as it comes directly from the parameter we use as input in the calculation, which can be seen from equation \ref{eq:2mt}. The minimum value of $M_{T2}$ for the process considered is actually given by $(M_{T2})_{min}^2 = m_l^2 + m_{\tilde{\chi}_1^0}^2$ but the smallest value $m_l$ can take is the mass of the electron which is negligible compared to $m_{\tilde{\chi}_1^0}$.
\\\\
In reality both the LSP mass and the slepton mass will be unknown as we do not know the exact point in parameter space of mSUGRA nature has potentially chosen, and therefore we can not simply measure the correct value of $m_{\tilde{l}_R}$ from the $M_{T2}$ distribution. We can only find $m_{\tilde{l}_R}$ as a function of the value of $m_{\tilde{\chi}_1^0}$ that we use as input in the calculation. If we calculate the $M_{T2}$ distribution for several different values of $m_{\tilde{\chi}_1^0}$, then the maximum value of $M_{T2}$ will be an estimate for the corresponding $m_{\tilde{l}_R}$ value. This is demonstrated in figure \ref{fig:mass}. We see that the relationship between the two masses behaves approximately as:
$$m_{\tilde{l}_R} \approx m_{\tilde{\chi}_1^0} + constant$$
\newpage
The example considered here shows the distribution of stransverse mass for a process at a parameter point where we get a good approximation to the mother particle mass. $M_{T2}$ will only be good at measuring particle masses if there are many events close to the maximum allowed value. This depends on the physics involved and the process considered, and will not necessarily be the case. For different processes and different parameter points the $M_{T2}$ distribution might give a poor approximation to the mass of the mother particles. For example if the signal processes have different types of pair produced particles with different masses and the decay chains are more complicated than the ones considered here, then the $M_{T2}$ distribution will be less strictly bound and more spread out. 
\\\\
This example is only a theoretical consideration of signal events. It will be necessary to also simulate SM background processes and experimental mis-measurements to analyze the usefulness of the variable for extracting particle masses. 

As long as the $M_{T2}$ distribution is different for SUSY events than for SM events, then it could lead to an excess in the distribution and it could be used to separate signal from background. Hence even if $M_{T2}$ is not useful for extracting particle masses, the variable could still be useful in the search for SUSY.

\section*{$M_{T2}$ as a discovery variable}
The idea of using $M_{T2}$ as a variable for discovering supersymmetry was first suggested in ref. \cite{Barr:2009wu} where they study how the properties of the variable might make it possible to distinguish new physics from SM background. The distribution of $M_{T2}$ reflects the masses of the pair produced particles, which will typically be much heavier for SUSY processes than for SM background processess. We therefore expect to get a signal region for large values of $M_{T2}$ where there is hardly any background.
The idea is that $M_{T2}$ could be used as a single-cut variable which is potentially sufficient in itself to discriminate against the majority of the background processes. 
\\\\
In this section we will perform a simulation of a SUSY signal from proton-proton collisions and calculate the $M_{T2}$ distribution for this signal, similarly to what has been done in ref. \cite{Barr:2009wu}, to show how the variable can be used in a search for new physics at the LHC. The SUSY signal will consist of pair produced strongly interacting sparticles, namely squarks and gluinos, where the end products will be jets and missing $p_T$.

In order to construct the $M_{T2}$ variable it is necessary to hypothesize the mass of the invisible particles. The SUSY particles will typically be massive but the true value will be unknown, and for SM background the invisible particles will be neutrinos which have approximately zero mass. The appropriate choice for the background processes is therefore to put $m_{\tilde{\chi}}=0$ in the calculation. This is also the only choice which guarantees that the inequality in equation \ref{eq:inequality} is preserved for all processes. 
This is what has been done in ref. \cite{Barr:2009wu}, and this is what we will do in our calculation as well. 
\\\\
There is a wide variety of SM background processes to be considered. One example is $t\bar{t}$ production. Since this is pair production of two particles with equal mass, $M_{T2}$ will be bounded from above by the mass of the top quark, $m_t$. This is not a strict bound in an experimental setting, but it is much smaller than the bound for SUSY particle masses which is what we search for. Since the top quark is the heaviest known SM particle, other SM events will have even smaller values of $M_{T2}$. For SUSY particles $M_{T2}$ is bounded from above by $m_0$, where $m_0$ is the mass of the heaviest of the pair produced SUSY particles. 

From this it follows that in order to have a signal region which is relatively free of background, we need a significant number of events with $M_{T2} > m_t$. If the true value of the invisible particle mass, $m_{\tilde{\chi}}$, is used in the calculation of $M_{T2}$, then this leads to a larger number of events close to the upper bound. Since we have chosen to put $m_{\tilde{\chi}} = 0,\: M_{T2}$ will obtain smaller values. However, we can see from equation \ref{eq:2mt} that as long as $m_{\tilde{\chi}} << |\mathbf{p}_T^{\tilde{\chi}}|$, then the events close to the upper bound when the true value of $m_{\tilde{\chi}}$ is used, will still be close to the upper bound when $m_{\tilde{\chi}}= 0$. We therefore hope to find a large number of signal events in the region approximately bounded by $m_t \lesssim M_{T2} \lesssim m_0$.
\\\\
The SUSY signal is simulated at the mSUGRA parameter point:
$$\tan\beta = 10,\: m_{1/2} = 250\: \text{GeV},\: m_0 = 100 \:\text{GeV},\: A_0 = -100\: \text{GeV},\: \mu > 0,$$ 
as calculated by SPheno-3.3.4. For this point we get a large production of squarks and gluinos at the center of mass energy of 7 TeV. 
\\\\
The process for squark pair production:
$$pp\rightarrow X + \tilde{q}\tilde{q} \rightarrow X + q\tilde{\chi}_1^0q\tilde{\chi}_1^0$$
results in two quarks, which lead to jets with typically high $p_T$ and low mass, and two of the lightest neutralinos, $\tilde{\chi}_1^0$, which are the LSP's for this model and therefore lead to missing $p_T$ in the detector. This process is therefore ideal for the $M_{T2}$ variable. 

We also get gluino pair production which is a more complicated process as the gluino has a longer decay chain:
$$pp\rightarrow X + \tilde{g}\tilde{g} \rightarrow X + q\tilde{q}q\tilde{q} \rightarrow qq\tilde{\chi}_1^0qq\tilde{\chi}_1^0$$
This process leads to four jets along with missing $p_T$. We could also of course get pair production of one gluino and one squark, in which case three jets will be produced. 

In our search we look for the two highest $p_T$ jets to use as input for the visible systems in the $M_{T2}$ calculation, which works well for the squark pair production process. For the gluino decay the largest $p_T$ jet will almost exclusively be the one coming from a squark and not the one coming directly from the gluino. This is because the mass difference between the squark and the gluino is small. The gluino mass is 615.6 GeV and the squark masses are approximately in the range $500 \lesssim m_{\tilde{q}} \lesssim 600$ GeV. This gives the quark decaying directly from the gluino very little recoil. The mass difference between the squark and the $\tilde{\chi}_1^0$, however, is large ($m_{\tilde{\chi}_1^0} = 97.1$ GeV) and hence the quark decaying from the squark gets a large recoil. This means that even for gluino production, $M_{T2}$ will effectively reflect the squark masses and not the gluino mass, as long as we choose the two largest $p_T$ jets as input. We see from the diagram below that this corresponds to only analysing the lower part of the diagram. 
\begin{align*}
\text{jet} \leftarrow q \leftarrow \tilde{g} & \tilde{g} \rightarrow q \rightarrow \text{jet}\\
\swarrow \:&\: \searrow\\
\text{jet} \leftarrow q \leftarrow \tilde{q} \:\:\:\:\:\:\:&\:\:\:\:\:\:\: \tilde{q} \rightarrow q \rightarrow \text{jet}\\
\swarrow \:\:\:\:\:\:\:\:\:\:&\:\:\:\:\:\:\:\:\:\: \searrow\\
\tilde{\chi}_1^0\:\:\:\:\:\:\:\:\:\:\:\:\:\:\:\:&\:\:\:\:\:\:\:\:\:\:\:\:\:\:\:\:\tilde{\chi}_1^0
\end{align*}

It would also be possible to form composite di-jet systems as input for the visible systems in $M_{T2}$ for the gluino decays. This would give a large number of events close to the gluino mass, which would be appropriate for mass determination. By only choosing the two largest $p_T$ jets however, although getting fewer near-maximal events, we get to combine several signal channels resulting in a larger signal sample.
From the simulation of hadron collisions we select events that contain at least two jets with $p_T > 50$ Gev. $M_{T2}$ is then calculated from the two highest $p_T$ jets in the event. These jets, though potentially massive, should have sufficiently small mass that putting $m^{vis} = 0$ in the calculation is a good approximation. The masses originate from strongly interacting SM particles and though they are non-zero, most of them are relatively small. We will therefore put $m^{vis} = 0$ when calculating $M_{T2}$.
\\\\
Approximately $2\times10^6$ signal events have been generated with PYTHIA 8.2. The resulting $M_{T2}$ distribution for these events is shown in figure \ref{fig:SUSYsignal}, where the distribution has been normalized to 100 $pb^{-1}$ in order to be able to compare with the results of ref. \cite{Barr:2009wu}.
\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{mt22.pdf}
  \caption{The $M_{T2}$ distribution for SUSY events with at least two jets with $p_T>50$ GeV. The gluino mass in the simulation is 615.6 GeV and the largest squark mass is 589.4 GeV. The value of $m_{\tilde{\chi}}$ is put to zero in the calculation of $M_{T2}$. This plot is linear in the y axis in the range $0 < y < 1$.
\label{fig:SUSYsignal}}
\end{figure}
\\\\\\
We see from figure \ref{fig:SUSYsignal} that the distribution of $M_{T2}$ for the SUSY signal has a smooth curve before it drops off quite abruptly at approximately 530 GeV. This corresponds well to the squark masses. Although there are very few events above this value, we do get some signal even for values larger than the gluino mass. In our definition of $M_{T2}$ we only considered two particle decay. However, when we choose the two largest $p_T$ jets, we do not know from where they originate. These large $M_{T2}$ values could therefore have been calculated from more than two original particles, in which case the upper bound does not apply as $M_{T2}$ is developed explicitly for the two-parent case.

Our distribution seems to correspond well to the results of ref. \cite{Barr:2009wu}. They have a smoother curve but it appears to have a small kink in the distribution at approximately 230 GeV. We have not considered any experimental detector effects in our simulation. If we had done so our distribution would probably have been smoother as well. They also have an abrupt endpoint at the value of the gluino mass but with huge error bars so this is consistent with our result.

We see that we get a significant amount of signal events well above $m_t$ which is the predicted theoretical upper bound for the background events. It will be necessary to simulate SM background processes as well to see if we actually get a signal region which is relatively free of background. There are background processes in which $M_{T2}$ is not bounded from above, such as processes with jets from initial state radiation or states originating from more than two massive or high-$p_T$ SM particles. The general amount of the background events are still expected to take low values of $M_{T2}$. 
Simulating background processes is, however, beyond the scope of this project. In order to compare the signal to background, a rough sketch is drawn into the plot showing what the total SM background is expected to look like based on the results of the simulations performed in ref. \cite{Barr:2009wu}. This comparison is shown in figure \ref{fig:background}. We see that the large majority of SM events have low $M_{T2}$ values. There are also SM events for values of $M_{T2} > m_t$ but these are well below the SUSY signal. 
\\\\
For this parameter point we therefore have a region of $M_{T2}$ which is signal-dominated. This result suggests that one could search for SUSY using a single cut requiring $M_{T2} > 230$ GeV combined with modest trigger requirements on jet $p_t$ or missing $p_T$.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.8\textwidth]{background.pdf}
  \caption{The $M_{T2}$ distribution for the SUSY signal shown as a black line and SM background events shown in green. The background distribution is a rough sketch based on the results in ref. \cite{Barr:2009wu}. This plot is linear in the y axis in the range $0 < y < 1$.
\label{fig:background}}
\end{figure}


%The distribution of $M_{T2}$ reflects the masses of the primary produced particles, which are much heavier for SUSY-processes than for SM background processess. Hence, new physics could appear as an excess in the tail of $M_{T2}$ \cite{Chatrchyan:2012jx}.

\section*{Conclusion}
In this project we have seen two examples of how the $M_{T2}$ variable can be used when searching for pair produced particles at hadron colliders where each particle decays to one visible and one invisible system in the detector. 
\\\\
In the first example we considered using the variable to measure the mass of the pair produced particles. The result looks promising for the model considered. The $M_{T2}$ distribution gets an endpoint which corresponds well to the mass of the mother particles. However, as long as the exact SUSY model is unkown, the mass of the invisible particles needs to be hypothesized in the calculation, and we only get the relationship between the two masses. So without knowing the invisible particle mass, we can not use $M_{T2}$ to directly measure the mass of the mother particles, but we can get a constraint for the two particle masses. 

The $M_{T2}$ distribution might give poorer results when extracting particle masses for different processes and different models, and so further simulations will be required for different cases. 

This example is purely theoretical and background processes have not been considered. The signal might be impossible to distinguish from background or the endpoint could prove difficult to measure, especially since it is not a vertical drop. It will be necessary to also simulate background processes and experimental errors in order to conclude the usefulness of $M_{T2}$ when it comes to measuring particle masses.
\\\\
In the second example we considered using $M_{T2}$ as a variable for discovering SUSY. Here also the results look promising for the model considered. We see that SM processes take smaller values for $M_{T2}$ than SUSY processes, and this leads to a region for large values of $M_{T2}$ which is clearly signal dominated. This example shows how $M_{T2}$ could be used for discovering new physics at hadron colliders. The variable discriminates against the majority of the SM background so no other cuts are required other than perhaps trigger requirements on jet $p_T$ and missing $p_T$.

Only R-parity conserving SUSY processes at a certain model point have been considered in this example, but other models where strongly interacting heavy particles are pair produced, and decay to jets and missing $p_T$, would result in similar final states as the signal, and could therefore be detected using the same analysis. Hence the search method is reasonably model independent.
\\\\
The simulations in this project suggest that if R-parity conserving SUSY exists close to the TeV scale, then the $M_{T2}$ variable could be used to detect SUSY processes at hadron colliders, and to extract the masses of the new particles. It will also be useful for other types of new physics with pair produced heavy particles resulting in similar final states. 

\newpage
\bibliography{mybib}{}
\bibliographystyle{auto_generated}

\end{document}
